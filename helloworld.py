def _print_hello():
    print('Hello World')

def _print_another_hello():
    print("Another Hello")

def main():
    _print_hello()

if __name__ == "__main__":
   main()